# Animapp
A Vue.js / Express.js web application for keeping track of and searching the animes you have watched.

# Setup

You need to have Node Js and yarn installed: https://nodejs.org/en/

You must at least have **NODE version 8.2.1**

### Client - Terminal A
```
cd client
yarn
yarn start
```

### Server - Terminal B
```
cd server
yarn
yarn start
```
