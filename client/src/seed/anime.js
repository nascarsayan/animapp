const animes = [{
  'alt_name': [
    'March comes in like a lion',
    'Sangatsu no Lion'
  ],
  'anime_id': '31646',
  'broadcast': 'Saturdays at 23:00 (JST)',
  'duration_mins': 25,
  'end_date': 'Mar 18, 2017',
  'genre': [
    'Game',
    'Slice of Life',
    'Drama',
    'Seinen'
  ],
  'licensor': [
    'Aniplex of America'
  ],
  'num_episodes': 22,
  'persona_crew': [
    {
      'crew': '15877',
      'persona': '21044'
    },
    {
      'crew': '185',
      'persona': '24312'
    },
    {
      'crew': '10765',
      'persona': '24311'
    },
    {
      'crew': '11641',
      'persona': '25275'
    },
    {
      'crew': '22',
      'persona': '144519'
    },
    {
      'crew': '158',
      'persona': '70293'
    },
    {
      'crew': '270',
      'persona': '70297'
    },
    {
      'crew': '79',
      'persona': '70313'
    },
    {
      'crew': '17',
      'persona': '31352'
    },
    {
      'crew': '58',
      'persona': '147823'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/12/82901.jpg',
  'premiered': 'Fall 2016',
  'primary_name': '3-gatsu no Lion',
  'producer': [
    'Aniplex',
    'Dentsu',
    'NHK',
    'Hakusensha',
    'Asmik Ace Entertainment',
    "Toy's Factory"
  ],
  'rating': 'PG-13 - Teens 13 or older',
  'source': 'Manga',
  'start_date': 'Oct 8, 2016',
  'status': 'Finished Airing',
  'studio': [
    'Shaft'
  ],
  'synopsis': "Having reached professional status in middle school, Rei Kiriyama is one of the few elite in the world of shogi. Due to this, he faces an enormous amount of pressure, both from the shogi community and his adoptive family. Seeking independence from his tense home life, he moves into an apartment in Tokyo. As a 17-year-old living on his own, Rei tends to take poor care of himself, and his reclusive personality ostracizes him from his peers in school and at the shogi hall. However, not long after his arrival in Tokyo, Rei meets Akari, Hinata, and Momo Kawamoto, a trio of sisters living with their grandfather who owns a traditional wagashi shop. Akari, the oldest of the three girls, is determined to combat Rei's loneliness and poorly sustained lifestyle with motherly hospitality. The Kawamoto sisters, coping with past tragedies, also share with Rei a unique familial bond that he has lacked for most of his life. As he struggles to maintain himself physically and mentally through his shogi career, Rei must learn how to interact with others and understand his own complex emotions. [Written by MAL Rewrite]",
  'type': 'TV'
}, {
  'alt_name': [
    'March comes in like a lion meets Bump of Chicken',
    'Sangatsu no Lion meets Bump of Chicken',
    'March Comes in Like a Lion meets Bump of Chicken',
    'Lion of March meets Bump of Chicken'
  ],
  'anime_id': '28789',
  'duration_mins': 5,
  'genre': [
    'Music',
    'Drama',
    'Seinen'
  ],
  'num_episodes': 1,
  'persona_crew': [],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/13/69921.jpg',
  'primary_name': '3-gatsu no Lion meets Bump of Chicken',
  'rating': 'G - All Ages',
  'source': 'Unknown',
  'start_date': 'Dec 8, 2014',
  'status': 'Finished Airing',
  'synopsis': "A CGI music video of Sangatsu no Lion set to Bump of Chicken's \"Fighter\" single. The website states that the collaboration started because the mangaka Umino Chika is a fan of Bump of Chicken and the members of Bump of Chicken are fans of Sangatsu no Lion . A special edition of the manga's 10th compiled volume shipped on November 28 with this single. (Source: ANN)",
  'type': 'Music'
}, {
  'alt_name': [
    'A.LI.CE',
    'alice'
  ],
  'anime_id': '3463',
  'duration_mins': 85,
  'genre': [
    'Sci-Fi'
  ],
  'num_episodes': 1,
  'persona_crew': [
    {
      'crew': '169',
      'persona': '13023'
    },
    {
      'crew': '275',
      'persona': '57807'
    },
    {
      'crew': '164',
      'persona': '57805'
    },
    {
      'crew': '518',
      'persona': '57813'
    },
    {
      'crew': '458',
      'persona': '57815'
    },
    {
      'crew': '284',
      'persona': '57811'
    },
    {
      'crew': '622',
      'persona': '57809'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/10/5762.jpg',
  'primary_name': 'A.LI.CE',
  'producer': [
    'GAGA'
  ],
  'rating': 'PG-13 - Teens 13 or older',
  'source': 'Unknown',
  'start_date': 'Feb 5, 2000',
  'status': 'Finished Airing',
  'synopsis': 'Alice, the youngest person ever to be sent into space, is the only survivor when her flight crashes back to earth. Not only is she the only survivor, but she learns that she has been mysteriously catapulted 30 years to the future, where a mysterious dictator known as Nero rules the world with and iron fist and has sent his soldiers to capture, or kill her. (Source: ANN)',
  'type': 'Movie'
}, {
  'anime_id': '8812',
  'broadcast': 'Unknown',
  'duration_mins': 19,
  'end_date': 'Jan 28, 1999',
  'genre': [
    'Adventure'
  ],
  'num_episodes': 28,
  'persona_crew': [
    {
      'crew': '439',
      'persona': '34049'
    },
    {
      'crew': '1412',
      'persona': '34047'
    },
    {
      'crew': '746',
      'persona': '34046'
    },
    {
      'crew': '478',
      'persona': '34051'
    },
    {
      'crew': '295',
      'persona': '34050'
    },
    {
      'crew': '8778',
      'persona': '34048'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/7/24323.jpg',
  'premiered': 'Spring 1998',
  'primary_name': 'Alice SOS',
  'producer': [
    'NHK'
  ],
  'rating': 'G - All Ages',
  'source': 'Original',
  'start_date': 'Apr 6, 1998',
  'status': 'Finished Airing',
  'studio': [
    'J.C.Staff'
  ],
  'synopsis': "Takashi loves reading books and especially loves \"Alice's Adventures in Wonderland\". One day, he buys some used books, and he finds one book thrown in. When Takashi opens the book, \"the God of Math\" M-1 appeared. Takashi is told that Alice is kidnapped, and he's asked to rescue her by M-1. And Takashi goes on an adventure in Wonderland to rescue Alice. (Source: ANN)",
  'type': 'TV'
}, {
  'alt_name': [
    'Alice Tanteikyoku',
    "Alice's Detective Agency"
  ],
  'anime_id': '18819',
  'broadcast': 'Unknown',
  'duration_mins': 12,
  'end_date': 'Jan 21, 1997',
  'genre': [
    'Mystery',
    'Kids',
    'Fantasy'
  ],
  'num_episodes': 56,
  'persona_crew': [
    {
      'crew': '41441',
      'persona': '25461'
    },
    {
      'crew': '5958',
      'persona': '41441'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/4/50211.jpg',
  'premiered': 'Spring 1995',
  'primary_name': 'Alice Tantei Kyoku',
  'producer': [
    'NHK',
    'Studio Junio'
  ],
  'rating': 'G - All Ages',
  'source': 'Original',
  'start_date': 'Apr 5, 1995',
  'status': 'Finished Airing',
  'synopsis': '',
  'type': 'TV'
}, {
  'anime_id': '5330',
  'duration_mins': 25,
  'genre': [
    'Action',
    'Sci-Fi'
  ],
  'num_episodes': 1,
  'persona_crew': [
    {
      'crew': '386',
      'persona': '109889'
    },
    {
      'crew': '458',
      'persona': '109883'
    },
    {
      'crew': '5648',
      'persona': '16996'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/12/10746.jpg',
  'primary_name': 'Alice in Cyberland',
  'producer': [
    'TV Tokyo',
    'Vega Entertainment',
    'Glams'
  ],
  'rating': 'PG-13 - Teens 13 or older',
  'source': 'Game',
  'start_date': 'Dec 21, 1996',
  'status': 'Finished Airing',
  'synopsis': 'When criminal Cybernackists appear in the Cyberland, the vast 21st century computer networks, Alice, Miss Catnick, Leina, and Julie are drawn in by the mysterious Lucia to become a fighting force of young girl soldiers in the Cyberland to combat the criminal element.',
  'type': 'OVA'
}, {
  'alt_name': [
    'Alice in Dream Land'
  ],
  'anime_id': '31941',
  'duration_mins': 44,
  'genre': [
    'Adventure',
    'Dementia',
    'Fantasy'
  ],
  'num_episodes': 1,
  'persona_crew': [],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/8/77671.jpg',
  'primary_name': 'Alice in Dreamland',
  'producer': [
    'Bitgang'
  ],
  'rating': 'G - All Ages',
  'source': 'Unknown',
  'start_date': 'Dec 19, 2015',
  'status': 'Finished Airing',
  'synopsis': '',
  'type': 'Movie'
}, {
  'alt_name': [
    'Alien Nine'
  ],
  'anime_id': '1177',
  'duration_mins': 28,
  'end_date': 'Feb 25, 2002',
  'genre': [
    'Sci-Fi',
    'Horror',
    'Psychological',
    'School'
  ],
  'licensor': [
    'Central Park Media'
  ],
  'num_episodes': 4,
  'persona_crew': [
    {
      'crew': '794',
      'persona': '7548'
    },
    {
      'crew': '169',
      'persona': '7549'
    },
    {
      'crew': '120',
      'persona': '7550'
    },
    {
      'crew': '259',
      'persona': '7553'
    },
    {
      'crew': '17',
      'persona': '44257'
    },
    {
      'crew': '80',
      'persona': '7551'
    },
    {
      'crew': '407',
      'persona': '24095'
    },
    {
      'crew': '1425',
      'persona': '7552'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/7/2607.jpg',
  'primary_name': 'Alien 9',
  'producer': [
    'Bandai Visual',
    'Genco',
    'MediaNet',
    'AT-X',
    'Nippon Columbia'
  ],
  'rating': 'R - 17+ (violence & profanity)',
  'source': 'Manga',
  'start_date': 'Jun 25, 2001',
  'status': 'Finished Airing',
  'studio': [
    'J.C.Staff'
  ],
  'synopsis': 'Yuri Ootani, a girl who has been afraid of aliens, has been chosen to be on the alien party with the class president Kumi Kawamura, whose only intention to join the alien party is to get out of presidential duties, as well as Kasumi Tomine who is perfect at everything she does, including fighting all the aliens that come in their way. But can they defeat a massive alien who has already abducted Kasumi?',
  'type': 'OVA'
}, {
  'anime_id': '17901',
  'duration_mins': 5,
  'genre': [
    'Fantasy',
    'Music'
  ],
  'num_episodes': 1,
  'persona_crew': [],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/6/47703.jpg',
  'primary_name': 'All Alone With You',
  'producer': [
    'Sony Music Entertainment'
  ],
  'rating': 'G - All Ages',
  'source': 'Music',
  'start_date': 'Mar 6, 2013',
  'status': 'Finished Airing',
  'studio': [
    '8bit'
  ],
  'synopsis': '"All Alone With You" is the fourth single by the music group EGOIST and is used as the second ending theme song of Psycho-Pass . The limited edition release includes a DVD with an animated music video.',
  'type': 'Music'
}, {
  'alt_name': [
    'All Out!!'
  ],
  'anime_id': '31588',
  'broadcast': 'Fridays at 00:00 (JST)',
  'duration_mins': 24,
  'end_date': 'Mar 31, 2017',
  'genre': [
    'Sports',
    'School',
    'Seinen'
  ],
  'licensor': [
    'Funimation'
  ],
  'num_episodes': 25,
  'persona_crew': [
    {
      'crew': '37569',
      'persona': '133410'
    },
    {
      'crew': '37562',
      'persona': '133409'
    },
    {
      'crew': '5626',
      'persona': '133411'
    },
    {
      'crew': '819',
      'persona': '133413'
    },
    {
      'crew': '12705',
      'persona': '133414'
    },
    {
      'crew': '5427',
      'persona': '146235'
    },
    {
      'crew': '270',
      'persona': '133415'
    },
    {
      'crew': '27405',
      'persona': '145008'
    },
    {
      'crew': '893',
      'persona': '143217'
    },
    {
      'crew': '15743',
      'persona': '133412'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/4/78649.jpg',
  'premiered': 'Fall 2016',
  'primary_name': 'All Out!!',
  'producer': [
    'Telecom Animation Film',
    'Mainichi Broadcasting System',
    'Kodansha',
    'Asmik Ace Entertainment',
    'Sony Music Entertainment'
  ],
  'rating': 'PG-13 - Teens 13 or older',
  'source': 'Manga',
  'start_date': 'Oct 7, 2016',
  'status': 'Finished Airing',
  'studio': [
    'Madhouse',
    'TMS Entertainment'
  ],
  'synopsis': 'In rugby, there is no ace striker, there is no number four batter, so who is the star of the team? The story begins at school entrance ceremony of Kanagawa High School where Kenji Gion, a small but gutsy go-getter joins the rugby club. He joins with his classmate, Iwashimizu, who has a complicated past and sub-captain Hachiouji, who always takes good care of his Club members. Lastly, there is Captain Sekizan, who has overwhelming powers but keeps his cards close to his chest. With such differences in both personality and physical performance, the team must learn to work and grow together so they can become the best. (Source: TMS Entertainment)',
  'type': 'TV'
}, {
  'alt_name': [
    'All That Gundam'
  ],
  'anime_id': '5270',
  'duration_mins': 1,
  'genre': [
    'Action',
    'Sci-Fi',
    'Mecha'
  ],
  'num_episodes': 1,
  'persona_crew': [
    {
      'crew': '33299',
      'persona': '2197'
    },
    {
      'crew': '5304',
      'persona': '33299'
    },
    {
      'crew': '8893',
      'persona': '5304'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/4/64997.jpg',
  'primary_name': 'All That Gundam',
  'producer': [
    'Atelier Musa'
  ],
  'rating': 'G - All Ages',
  'source': 'Unknown',
  'start_date': '1989',
  'status': 'Finished Airing',
  'studio': [
    'Sunrise'
  ],
  'synopsis': 'Animated video created just for a 1989 event that featured all the major mobile suit robots from the first 10 years of the Gundam anime franchise. (Source: ANN)',
  'type': 'Special'
}, {
  'alt_name': [
    'Allison & Lillia',
    'Alison and Lilia',
    'Arison to Riria',
    'Allison and Lillia'
  ],
  'anime_id': '3549',
  'broadcast': 'Unknown',
  'duration_mins': 24,
  'end_date': 'Oct 2, 2008',
  'genre': [
    'Action',
    'Adventure',
    'Shounen'
  ],
  'licensor': [
    'Sentai Filmworks'
  ],
  'num_episodes': 26,
  'persona_crew': [
    {
      'crew': '57',
      'persona': '10009'
    },
    {
      'crew': '6',
      'persona': '10121'
    },
    {
      'crew': '81',
      'persona': '12325'
    },
    {
      'crew': '283',
      'persona': '12316'
    },
    {
      'crew': '40',
      'persona': '10120'
    },
    {
      'crew': '11',
      'persona': '10119'
    },
    {
      'crew': '823',
      'persona': '17168'
    },
    {
      'crew': '61',
      'persona': '13911'
    },
    {
      'crew': '207',
      'persona': '42139'
    },
    {
      'crew': '234',
      'persona': '40977'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/9/50491.jpg',
  'premiered': 'Spring 2008',
  'primary_name': 'Allison to Lillia',
  'rating': 'PG-13 - Teens 13 or older',
  'source': 'Light novel',
  'start_date': 'Apr 3, 2008',
  'status': 'Finished Airing',
  'studio': [
    'Madhouse'
  ],
  'synopsis': 'Set in a continent divided into two commonwealths that have been engaged in war for hundreds of years, Allison and Will go on a mission to search "the treasure that will put an end to the war". Their hope is inherited to their daughter Lillia, who strives to thaw the torn nations into a united country. This anime encourages young generations to believe in a world without hatred or war regardless of nationalities and beliefs. (Source: Anime Network)',
  'type': 'TV'
}, {
  'alt_name': [
    'Aloha! Youkai Watch: Rakuen Hawaii de Geragerapo~!!',
    'Hawaii Tourism Board'
  ],
  'anime_id': '31164',
  'duration_mins': 2,
  'genre': [
    'Slice of Life',
    'Kids',
    'Supernatural'
  ],
  'num_episodes': 1,
  'persona_crew': [],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/12/74922.jpg',
  'primary_name': 'Aloha! Youkai Watch: Rakuen Hawaii de Geragerapou!!',
  'rating': 'G - All Ages',
  'source': 'Game',
  'start_date': 'Jun 24, 2015',
  'status': 'Finished Airing',
  'synopsis': "A commissioned anime by the Hawaii Tourism Authority. Jibanyan introduces the attractions of the four main islands of Hawaii. The short features new Hawaiian versions of youkai. The famous locales in Hawaii featured include Kauai's Waimea Canyon, Oahu's Diamond Head, Maui's Waikiki Beach, and Hawaii's Haleakal\u0101 National Park. The youkai and kids try local cuisine and shopping. (Source: ANN)",
  'type': 'ONA'
}, {
  'alt_name': [
    'Sekai Meisaku Gekijou',
    'Alps Stories: My Annette'
  ],
  'anime_id': '2546',
  'broadcast': 'Sundays at 19:30 (JST)',
  'duration_mins': 25,
  'end_date': 'Dec 25, 1983',
  'genre': [
    'Slice of Life',
    'Historical',
    'Drama'
  ],
  'num_episodes': 48,
  'persona_crew': [
    {
      'crew': '760',
      'persona': '39166'
    },
    {
      'crew': '5763',
      'persona': '39170'
    },
    {
      'crew': '288',
      'persona': '39168'
    },
    {
      'crew': '1157',
      'persona': '39169'
    },
    {
      'crew': '10373',
      'persona': '39167'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/5/3208.jpg',
  'premiered': 'Winter 1983',
  'primary_name': 'Alps Monogatari: Watashi no Annette',
  'rating': 'G - All Ages',
  'source': 'Unknown',
  'start_date': 'Jan 9, 1983',
  'status': 'Finished Airing',
  'studio': [
    'Nippon Animation'
  ],
  'synopsis': "Annette was born into a poor family in Switzerland. She is a sweet girl and takes good care of her little brother Dannie. Lucien is her best friend. One day Dannie gets injured and develops a limp. The accident was Lucien's fault. The happy days turn into a long period of suffering, but little by little Annette and Lucien regain their lost friendship.",
  'type': 'TV'
}, {
  'alt_name': [
    'Sekai Meisaku Gekijou Kanketsu Ban: Alps Monogatari Watashi no Annette',
    'Alps Monogatari: Watashi no Annette Recaps'
  ],
  'anime_id': '24647',
  'duration_mins': 45,
  'genre': [
    'Slice of Life',
    'Drama'
  ],
  'num_episodes': 2,
  'persona_crew': [],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/8/63437.jpg',
  'primary_name': 'Alps Monogatari: Watashi no Annette Specials',
  'rating': 'G - All Ages',
  'source': 'Unknown',
  'start_date': 'Apr, 2001',
  'status': 'Finished Airing',
  'studio': [
    'Nippon Animation'
  ],
  'synopsis': 'A summary of the Alps Monogatari: Watashi no Annette full series aired as a TV specials.',
  'type': 'Special'
}, {
  'alt_name': [
    'Heidi: Girl of the Alps'
  ],
  'anime_id': '2225',
  'broadcast': 'Sundays at 19:30 (JST)',
  'duration_mins': 25,
  'end_date': 'Dec 29, 1974',
  'genre': [
    'Drama',
    'Historical',
    'Slice of Life'
  ],
  'num_episodes': 52,
  'persona_crew': [
    {
      'crew': '786',
      'persona': '8827'
    },
    {
      'crew': '9188',
      'persona': '30308'
    },
    {
      'crew': '1151',
      'persona': '8828'
    },
    {
      'crew': '920',
      'persona': '17787'
    },
    {
      'crew': '1542',
      'persona': '30307'
    },
    {
      'crew': '557',
      'persona': '155029'
    },
    {
      'crew': '1237',
      'persona': '147780'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/1/2420.jpg',
  'premiered': 'Winter 1974',
  'primary_name': 'Alps no Shoujo Heidi',
  'producer': [
    'Fuji TV'
  ],
  'rating': 'PG - Children',
  'source': 'Book',
  'start_date': 'Jan 6, 1974',
  'status': 'Finished Airing',
  'studio': [
    'Nippon Animation'
  ],
  'synopsis': "After becoming an orphan, Heidi is forced to live with her grandfather \u00d6hi, who lives in the Alps. She learns he's a very bitter man who only accepted by force to take her in. But Heidi's kindness may be able to open his heart. Together with the shepherd Peter and invalid Klara, she has lots of adventures. (Source: adapted from ANN)",
  'type': 'TV'
}, {
  'alt_name': [
    'The Story of Heidi'
  ],
  'anime_id': '9547',
  'duration_mins': 107,
  'genre': [
    'Drama',
    'Historical',
    'Slice of Life'
  ],
  'num_episodes': 1,
  'persona_crew': [
    {
      'crew': '11839',
      'persona': '41408'
    },
    {
      'crew': '45545',
      'persona': '11839'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/6/26447.jpg',
  'primary_name': 'Alps no Shoujo Heidi (1979)',
  'rating': 'PG - Children',
  'source': 'Unknown',
  'start_date': 'Mar 17, 1979',
  'status': 'Finished Airing',
  'studio': [
    'Nippon Animation'
  ],
  'synopsis': 'Condensed version of the entire TV series where all the voice actors, except for Heidi and the grandfather, were replaced.',
  'type': 'Movie'
}, {
  'alt_name': [
    'Heidi: Girl of the Alps Pilot'
  ],
  'anime_id': '10813',
  'duration_mins': 5,
  'genre': [
    'Comedy',
    'Drama',
    'Slice of Life'
  ],
  'num_episodes': 1,
  'persona_crew': [],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/2/29867.jpg',
  'primary_name': 'Alps no Shoujo Heidi Pilot',
  'producer': [
    'TCJ'
  ],
  'rating': 'G - All Ages',
  'source': 'Unknown',
  'start_date': '1967',
  'status': 'Finished Airing',
  'synopsis': 'Pilot episode of the famous anime Heidi.',
  'type': 'Special'
}, {
  'anime_id': '9548',
  'duration_mins': 88,
  'genre': [
    'Slice of Life',
    'Historical',
    'Drama'
  ],
  'num_episodes': 1,
  'persona_crew': [
    {
      'crew': '45545',
      'persona': '11839'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/6/26448.jpg',
  'primary_name': 'Alps no Shoujo Heidi: Alm no Yama-hen',
  'rating': 'PG - Children',
  'source': 'Unknown',
  'start_date': 'Aug 21, 1996',
  'status': 'Finished Airing',
  'studio': [
    'Nippon Animation'
  ],
  'synopsis': 'Condensed version of the first half of the TV series.',
  'type': 'Movie'
}, {
  'anime_id': '9549',
  'duration_mins': 89,
  'genre': [
    'Slice of Life',
    'Historical',
    'Drama'
  ],
  'num_episodes': 1,
  'persona_crew': [
    {
      'crew': '45545',
      'persona': '11839'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/9/26449.jpg',
  'primary_name': 'Alps no Shoujo Heidi: Heidi to Clara-hen',
  'rating': 'PG - Children',
  'source': 'Unknown',
  'start_date': 'Aug 21, 1996',
  'status': 'Finished Airing',
  'studio': [
    'Nippon Animation'
  ],
  'synopsis': 'Condensed version of the second half of the TV series.',
  'type': 'Movie'
}, {
  'alt_name': [
    'Sweetness & Lightning'
  ],
  'anime_id': '32828',
  'broadcast': 'Tuesdays at 01:05 (JST)',
  'duration_mins': 24,
  'end_date': 'Sep 20, 2016',
  'genre': [
    'Slice of Life',
    'Comedy',
    'Seinen'
  ],
  'num_episodes': 12,
  'persona_crew': [
    {
      'crew': '31391',
      'persona': '130917'
    },
    {
      'crew': '869',
      'persona': '130918'
    },
    {
      'crew': '513',
      'persona': '130916'
    },
    {
      'crew': '1',
      'persona': '140412'
    },
    {
      'crew': '890',
      'persona': '140411'
    },
    {
      'crew': '10765',
      'persona': '141986'
    },
    {
      'crew': '41572',
      'persona': '141988'
    },
    {
      'crew': '41573',
      'persona': '141989'
    },
    {
      'crew': '41571',
      'persona': '141987'
    },
    {
      'crew': '124',
      'persona': '141985'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/6/80546.jpg',
  'premiered': 'Summer 2016',
  'primary_name': 'Amaama to Inazuma',
  'producer': [
    'Toei Video',
    'Kodansha',
    'Shin-Ei Animation',
    'DAX Production',
    'Asmik Ace Entertainment',
    'Yomiuri TV Enterprise',
    'Sumitomo'
  ],
  'rating': 'PG-13 - Teens 13 or older',
  'source': 'Manga',
  'start_date': 'Jul 5, 2016',
  'status': 'Finished Airing',
  'studio': [
    'TMS Entertainment'
  ],
  'synopsis': "Since the death of his wife, Kouhei Inuzuka has been caring for his young daughter Tsumugi to the best of his abilities. However, with his lack of culinary knowledge and his busy job as a teacher, he is left relying on ready-made meals from convenience stores to feed the little girl. Frustrated at his own incapability to provide a fresh, nutritious meal for his daughter, Kouhei takes up an offer from his student, Kotori Iida, to come have dinner at her family's restaurant. But on their very first visit, the father and daughter discover that the restaurant is often closed due to Kotori's mother being away for work and that Kotori often eats alone. After much pleading from his pupil, Kouhei decides to continue to go to the restaurant with Tsumugi to cook and share delicious homemade food with Kotori. Amaama to Inazuma follows the heartwarming story of a caring father trying his hardest to make his adorable little daughter happy, while exploring the meanings and values behind cooking, family, and the warm meals at home that are often taken for granted. [Written by MAL Rewrite]",
  'type': 'TV'
}, {
  'alt_name': [
    'Mario OVA'
  ],
  'anime_id': '4389',
  'duration_mins': 11,
  'genre': [
    'Adventure',
    'Comedy',
    'Shounen'
  ],
  'num_episodes': 3,
  'persona_crew': [
    {
      'crew': '326',
      'persona': '9144'
    },
    {
      'crew': '8762',
      'persona': '9146'
    },
    {
      'crew': '5386',
      'persona': '13883'
    },
    {
      'crew': '1522',
      'persona': '9145'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/12/8326.jpg',
  'primary_name': 'Amada Anime Series: Super Mario Brothers',
  'producer': [
    'Studio Junio'
  ],
  'rating': 'PG - Children',
  'source': 'Game',
  'start_date': 'Aug 3, 1989',
  'status': 'Finished Airing',
  'synopsis': 'The Super Mario Amada Series are a series of short Japanese fairy tales anime that is found and released only in Japan. The series are released in August 3, 1989 once again only in Japan; making it extremely rare to people not living in Japan. The series is based on one of the three Japanese fairy tales that are told to children. Additionally, the series contained Mario series that are played in the three short Japanese fairy tales. The characters introduced Mario, Luigi, Princess Peach, Bowser, and his Koopalings during the fairy tales. Each fairy tale shown in anime consists of about 15 minutes. The three Japanese fairy tales contain; Momotaro, Issunboshi, and Snow White. (Source: AniDB)',
  'type': 'OVA'
}, {
  'alt_name': [
    'Ah My Buddha',
    'Amaenaideyo!!'
  ],
  'anime_id': '591',
  'broadcast': 'Unknown',
  'duration_mins': 24,
  'end_date': 'Sep 16, 2005',
  'genre': [
    'Comedy',
    'Ecchi',
    'Harem',
    'Romance',
    'Supernatural'
  ],
  'licensor': [
    'Nozomi Entertainment',
    'Media Blasters'
  ],
  'num_episodes': 12,
  'persona_crew': [
    {
      'crew': '31',
      'persona': '5015'
    },
    {
      'crew': '164',
      'persona': '4826'
    },
    {
      'crew': '315',
      'persona': '5019'
    },
    {
      'crew': '94',
      'persona': '5018'
    },
    {
      'crew': '50',
      'persona': '5016'
    },
    {
      'crew': '71',
      'persona': '5017'
    },
    {
      'crew': '98',
      'persona': '107699'
    },
    {
      'crew': '81',
      'persona': '28059'
    },
    {
      'crew': '1501',
      'persona': '5020'
    },
    {
      'crew': '143',
      'persona': '15794'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/2/32071.jpg',
  'premiered': 'Summer 2005',
  'primary_name': 'Amaenaide yo!!',
  'rating': 'R+ - Mild Nudity',
  'source': 'Manga',
  'start_date': 'Jul 1, 2005',
  'status': 'Finished Airing',
  'studio': [
    'Studio Deen'
  ],
  'synopsis': 'Satonaka Ikkou, a 16 year old boy, is a first year trainee at the Saienji Buddhist Temple. He was sent there by his parents to be trained by his grandmother, the Saienji Priestess. At the temple he finds himself surrounded by beautiful female priestesses-in-training. Upon seeing a girl naked, Ikko has the ability to turn into a super-monk, performing massive exorcisms for the good of the temple. (Source: ANN)',
  'type': 'TV'
}, {
  'alt_name': [
    'Ah My Buddha Katsu',
    'Amaenaideyo!! 2',
    'Amaenaideyo!! Katsu'
  ],
  'anime_id': '886',
  'broadcast': 'Unknown',
  'duration_mins': 24,
  'end_date': 'Mar 22, 2006',
  'genre': [
    'Comedy',
    'Ecchi',
    'Harem',
    'Romance',
    'Supernatural'
  ],
  'licensor': [
    'Nozomi Entertainment',
    'Media Blasters'
  ],
  'num_episodes': 12,
  'persona_crew': [
    {
      'crew': '126',
      'persona': '5551'
    },
    {
      'crew': '31',
      'persona': '5015'
    },
    {
      'crew': '164',
      'persona': '4826'
    },
    {
      'crew': '315',
      'persona': '5019'
    },
    {
      'crew': '94',
      'persona': '5018'
    },
    {
      'crew': '50',
      'persona': '5016'
    },
    {
      'crew': '71',
      'persona': '5017'
    },
    {
      'crew': '9',
      'persona': '23122'
    },
    {
      'crew': '1436',
      'persona': '23123'
    },
    {
      'crew': '98',
      'persona': '107699'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/2/15153.jpg',
  'premiered': 'Winter 2006',
  'primary_name': 'Amaenaide yo!! Katsu!!',
  'rating': 'R+ - Mild Nudity',
  'source': 'Manga',
  'start_date': 'Jan 4, 2006',
  'status': 'Finished Airing',
  'studio': [
    'Studio Deen'
  ],
  'synopsis': 'In this sequel, a girl named Kazusano Kazuki join up with the 6 girls and Ikko. With Kazuki around, Ikko will find himself in more embarassing situations with these priestesses-in-training. (Source: ANN)',
  'type': 'TV'
}, {
  'alt_name': [
    'Your Lie in April'
  ],
  'anime_id': '23273',
  'broadcast': 'Fridays at 01:20 (JST)',
  'duration_mins': 22,
  'end_date': 'Mar 20, 2015',
  'genre': [
    'Drama',
    'Music',
    'Romance',
    'School',
    'Shounen'
  ],
  'licensor': [
    'Aniplex of America'
  ],
  'num_episodes': 22,
  'persona_crew': [
    {
      'crew': '16135',
      'persona': '69411'
    },
    {
      'crew': '16635',
      'persona': '69407'
    },
    {
      'crew': '11622',
      'persona': '69409'
    },
    {
      'crew': '15743',
      'persona': '69405'
    },
    {
      'crew': '869',
      'persona': '85841'
    },
    {
      'crew': '11297',
      'persona': '120711'
    },
    {
      'crew': '10765',
      'persona': '114053'
    },
    {
      'crew': '672',
      'persona': '85839'
    },
    {
      'crew': '602',
      'persona': '114051'
    },
    {
      'crew': '28755',
      'persona': '81725'
    }
  ],
  'pic_url': 'https://myanimelist.cdn-dena.com/images/anime/3/67177.jpg',
  'premiered': 'Fall 2014',
  'primary_name': 'Shigatsu wa Kimi no Uso',
  'producer': [
    'Aniplex',
    'Dentsu',
    'Kodansha',
    'Fuji TV',
    'Kyoraku Industrial Holdings'
  ],
  'rating': 'PG-13 - Teens 13 or older',
  'source': 'Manga',
  'start_date': 'Oct 10, 2014',
  'status': 'Finished Airing',
  'studio': [
    'A-1 Pictures'
  ],
  'synopsis': "Music accompanies the path of the human metronome, the prodigious pianist Kousei Arima. But after the passing of his mother, Saki Arima, Kousei falls into a downward spiral, rendering him unable to hear the sound of his own piano. Two years later, Kousei still avoids the piano, leaving behind his admirers and rivals, and lives a colorless life alongside his friends Tsubaki Sawabe and Ryouta Watari. However, everything changes when he meets a beautiful violinist, Kaori Miyazono, who stirs up his world and sets him on a journey to face music again. Based on the manga series of the same name, Shigatsu wa Kimi no Uso approaches the story of Kousei's recovery as he discovers that music is more than playing each note perfectly, and a single melody can bring in the fresh spring air of April. [Written by MAL Rewrite]",
  'type': 'TV'
}]
module.exports = animes
